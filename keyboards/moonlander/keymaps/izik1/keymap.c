#include QMK_KEYBOARD_H
#include "version.h"
#include "keymap_german.h"
#include "keymap_french.h"
#include "keymap_spanish.h"
#include "keymap_german_ch.h"
#include "keymap_jp.h"
#include "keymap_contributions.h"
#include "keymap_steno.h"

#define MOON_LED_LEVEL LED_LEVEL

enum layers {
    BASE_LIN,
    BASE_MAC,
    ONESHOT,
    GAMING,
    SYMBOLS,
    STENO,
    MEDIA,
    LOWEST_UNUSED
};

enum custom_keycodes {
  RGB_SLD = ML_SAFE_RANGE,
  HSV_0_255_255,
  HSV_86_255_128,
  HSV_172_255_255,
};


enum tap_dance_codes {
    DANCE_LCTL_EQUAL,
    DANCE_LCTL_DELETE,
    DANCE_LCTL_Q,
    DANCE_LCTL_W,
    DANCE_LCTL_E,
    DANCE_LCTL_R,
    DANCE_LCTL_T,
    DANCE_LCTL_BSPACE,
    DANCE_LCTL_A,
    DANCE_LCTL_S,
    DANCE_LCTL_D,
    DANCE_LCTL_F,
    DANCE_LCTL_G,
    DANCE_LCTL_Z,
    DANCE_LCTL_X,
    DANCE_LCTL_C,
    DANCE_LCTL_V,
    DANCE_LCTL_B,
    DANCE_LCTL_GRAVE,
    DANCE_LCTL_LEFT,
    DANCE_LCTL_RIGHT,
    DANCE_LCTL_MINUS,
    DANCE_LCTL_TAB,
    DANCE_LCTL_Y,
    DANCE_LCTL_U,
    DANCE_LCTL_I,
    DANCE_LCTL_O,
    DANCE_LCTL_P,
    DANCE_LCTL_BSLASH,
    DANCE_LCTL_H,
    DANCE_LCTL_J,
    DANCE_LCTL_K,
    DANCE_LCTL_L,
    DANCE_LCTL_SCOLON,
    DANCE_LCTL_QUOTE,
    DANCE_LCTL_N,
    DANCE_LCTL_M,
    DANCE_LCTL_COMMA,
    DANCE_LCTL_DOT,
    DANCE_LCTL_SLASH,
    DANCE_LCTL_UP,
    DANCE_LCTL_DOWN,
    DANCE_LCTL_LBRACKET,
    DANCE_LCTL_RBRACKET,
    DANCE_LCMD_EQUAL,
    DANCE_LCMD_DELETE,
    DANCE_LCMD_Q,
    DANCE_LCMD_W,
    DANCE_LCMD_E,
    DANCE_LCMD_R,
    DANCE_LCMD_T,
    DANCE_LCMD_BSPACE,
    DANCE_LCMD_A,
    DANCE_LCMD_S,
    DANCE_LCMD_D,
    DANCE_LCMD_F,
    DANCE_LCMD_G,
    DANCE_LCMD_Z,
    DANCE_LCMD_X,
    DANCE_LCMD_C,
    DANCE_LCMD_V,
    DANCE_LCMD_B,
    DANCE_LCMD_GRAVE,
    DANCE_LCMD_LEFT,
    DANCE_LCMD_RIGHT,
    DANCE_LCMD_MINUS,
    DANCE_LCMD_TAB,
    DANCE_LCMD_Y,
    DANCE_LCMD_U,
    DANCE_LCMD_I,
    DANCE_LCMD_O,
    DANCE_LCMD_P,
    DANCE_LCMD_BSLASH,
    DANCE_LCMD_H,
    DANCE_LCMD_J,
    DANCE_LCMD_K,
    DANCE_LCMD_L,
    DANCE_LCMD_SCOLON,
    DANCE_LCMD_QUOTE,
    DANCE_LCMD_N,
    DANCE_LCMD_M,
    DANCE_LCMD_COMMA,
    DANCE_LCMD_DOT,
    DANCE_LCMD_SLASH,
    DANCE_LCMD_UP,
    DANCE_LCMD_DOWN,
    DANCE_LCMD_LBRACKET,
    DANCE_LCMD_RBRACKET,
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
  [BASE_LIN] = LAYOUT_moonlander(
    TD(DANCE_LCTL_EQUAL),     KC_1,                KC_2,                KC_3,                 KC_4,                   KC_5,                    KC_LEFT,                       KC_RIGHT,              KC_6,                    KC_7,                KC_8,                   KC_9,                      KC_0,                      TD(DANCE_LCTL_MINUS),
    TD(DANCE_LCTL_DELETE),    TD(DANCE_LCTL_Q),    TD(DANCE_LCTL_W),    TD(DANCE_LCTL_E),     TD(DANCE_LCTL_R),       TD(DANCE_LCTL_T),        KC_ESCAPE,                     TD(DANCE_LCTL_TAB),    TD(DANCE_LCTL_Y),        TD(DANCE_LCTL_U),    TD(DANCE_LCTL_I),       TD(DANCE_LCTL_O),          TD(DANCE_LCTL_P),          TD(DANCE_LCTL_BSLASH),
    TD(DANCE_LCTL_BSPACE),    TD(DANCE_LCTL_A),    TD(DANCE_LCTL_S),    TD(DANCE_LCTL_D),     TD(DANCE_LCTL_F),       TD(DANCE_LCTL_G),        KC_LCTRL,                      KC_RCTRL,              TD(DANCE_LCTL_H),        TD(DANCE_LCTL_J),    TD(DANCE_LCTL_K),       TD(DANCE_LCTL_L),          TD(DANCE_LCTL_SCOLON),     TD(DANCE_LCTL_QUOTE),
    KC_LSHIFT,                TD(DANCE_LCTL_Z),    TD(DANCE_LCTL_X),    TD(DANCE_LCTL_C),     TD(DANCE_LCTL_V),       TD(DANCE_LCTL_B),                                                              TD(DANCE_LCTL_N),        TD(DANCE_LCTL_M),    TD(DANCE_LCTL_COMMA),   TD(DANCE_LCTL_DOT),        TD(DANCE_LCTL_SLASH),      KC_RSHIFT,
    TD(DANCE_LCTL_GRAVE) ,    OSL(ONESHOT),        KC_LALT,             TD(DANCE_LCTL_LEFT),  TD(DANCE_LCTL_RIGHT),   LALT_T(KC_APPLICATION),                                                        KC_ESCAPE,               TD(DANCE_LCTL_UP),   TD(DANCE_LCTL_DOWN),    TD(DANCE_LCTL_LBRACKET),   TD(DANCE_LCTL_RBRACKET),   DF(BASE_MAC),
    KC_SPACE,                 KC_BSPACE,           KC_LGUI,                                                                                                                                                                                                                KC_LALT,                   KC_TAB,                    KC_ENTER
  ),
  [BASE_MAC] = LAYOUT_moonlander(
    TD(DANCE_LCMD_EQUAL),     KC_1,                KC_2,                KC_3,                 KC_4,                   KC_5,                    KC_LEFT,                       KC_RIGHT,              KC_6,                    KC_7,                KC_8,                   KC_9,                      KC_0,                      TD(DANCE_LCMD_MINUS),
    TD(DANCE_LCMD_DELETE),    TD(DANCE_LCMD_Q),    TD(DANCE_LCMD_W),    TD(DANCE_LCMD_E),     TD(DANCE_LCMD_R),       TD(DANCE_LCMD_T),        KC_ESCAPE,                     TD(DANCE_LCMD_TAB),    TD(DANCE_LCMD_Y),        TD(DANCE_LCMD_U),    TD(DANCE_LCMD_I),       TD(DANCE_LCMD_O),          TD(DANCE_LCMD_P),          TD(DANCE_LCMD_BSLASH),
    TD(DANCE_LCMD_BSPACE),    TD(DANCE_LCMD_A),    TD(DANCE_LCMD_S),    TD(DANCE_LCMD_D),     TD(DANCE_LCMD_F),       TD(DANCE_LCMD_G),        KC_LCTRL,                      KC_RCTRL,              TD(DANCE_LCMD_H),        TD(DANCE_LCMD_J),    TD(DANCE_LCMD_K),       TD(DANCE_LCMD_L),          TD(DANCE_LCMD_SCOLON),     TD(DANCE_LCMD_QUOTE),
    KC_LSHIFT,                TD(DANCE_LCMD_Z),    TD(DANCE_LCMD_X),    TD(DANCE_LCMD_C),     TD(DANCE_LCMD_V),       TD(DANCE_LCMD_B),                                                              TD(DANCE_LCMD_N),        TD(DANCE_LCMD_M),    TD(DANCE_LCMD_COMMA),   TD(DANCE_LCMD_DOT),        TD(DANCE_LCMD_SLASH),      KC_RSHIFT,
    TD(DANCE_LCMD_GRAVE) ,    OSL(ONESHOT),        KC_LALT,             TD(DANCE_LCMD_LEFT),  TD(DANCE_LCMD_RIGHT),   LALT_T(KC_APPLICATION),                                                        KC_ESCAPE,               TD(DANCE_LCMD_UP),   TD(DANCE_LCMD_DOWN),    TD(DANCE_LCMD_LBRACKET),   TD(DANCE_LCMD_RBRACKET),   DF(BASE_LIN),
    KC_SPACE,                 KC_BSPACE,           KC_LGUI,                                                                                                                                                                                                                KC_LALT,                   KC_TAB,                    KC_ENTER
  ),
  [ONESHOT] = LAYOUT_moonlander(
    KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,   KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,                                 KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,
    KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, TO(GAMING),       KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,                                 KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,
    KC_TRANSPARENT, KC_TRANSPARENT, TO(SYMBOLS),    TO(STENO),        TO(MEDIA),      KC_TRANSPARENT, KC_NO,                                          KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_PSCREEN,     KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,
    KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, MAGIC_TOGGLE_NKRO,KC_TRANSPARENT, KC_TRANSPARENT,                                                                 KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,
    KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,   KC_TRANSPARENT, KC_TRANSPARENT,                                                                                                 KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,
    KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,                   KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT
  ),
  [GAMING] = LAYOUT_moonlander(
    KC_EQUAL,       KC_1,           KC_2,           KC_3,           KC_4,           KC_5,           KC_TRANSPARENT,                                 KC_TRANSPARENT, KC_6,           KC_7,           KC_8,           KC_9,           KC_0,           KC_MINUS,
    KC_DELETE,      KC_Q,           KC_W,           KC_E,           KC_R,           KC_T,           KC_TRANSPARENT,                                 KC_ESCAPE,      KC_Y,           KC_U,           KC_I,           KC_O,           KC_P,           KC_BSLASH,
    KC_BSPACE,      KC_A,           KC_S,           KC_D,           KC_F,           KC_G,           TG(GAMING),                                     KC_RCTRL,       KC_H,           KC_J,           KC_K,           KC_L,           KC_SCOLON,      KC_QUOTE,
    KC_LSHIFT,      KC_Z,           KC_X,           KC_C,           KC_V,           KC_B,                                           KC_N,           KC_M,           KC_COMMA,       KC_DOT,         KC_SLASH,       KC_RSHIFT,
    KC_GRAVE,       KC_TRANSPARENT, KC_TRANSPARENT, LALT_T(KC_NO),  KC_LCTRL,       KC_TRANSPARENT,                                                                                                 KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_LBRACKET,    KC_RBRACKET,    KC_TRANSPARENT,
    KC_SPACE,       KC_BSPACE,      KC_LGUI,                        KC_LALT,        KC_TAB,         KC_ENTER
  ),
  [SYMBOLS] = LAYOUT_moonlander(
    KC_ESCAPE,      KC_F1,          KC_F2,          KC_F3,          KC_F4,          KC_F5,          KC_TRANSPARENT,                                 KC_TRANSPARENT, KC_F6,          KC_F7,          KC_F8,          KC_F9,          KC_F10,         KC_F11,
    KC_TRANSPARENT, KC_EXLM,        KC_AT,          KC_LCBR,        KC_RCBR,        KC_PIPE,        KC_TRANSPARENT,                                 KC_TRANSPARENT, KC_UP,          KC_7,           KC_8,           KC_9,           KC_ASTR,        KC_F12,
    KC_TRANSPARENT, KC_HASH,        KC_DLR,         KC_LPRN,        KC_RPRN,        KC_GRAVE,       TG(SYMBOLS),                                    KC_TRANSPARENT, KC_DOWN,        KC_4,           KC_5,           KC_6,           KC_KP_PLUS,     KC_TRANSPARENT,
    KC_TRANSPARENT, KC_PERC,        KC_CIRC,        KC_LBRACKET,    KC_RBRACKET,    KC_TILD,                                        KC_AMPR,        KC_1,           KC_2,           KC_3,           KC_BSLASH,      KC_TRANSPARENT,
    KC_TRANSPARENT, KC_COMMA,       HSV_0_255_255,  HSV_86_255_128, HSV_172_255_255,RGB_MOD,                                                                                                        RGB_TOG,        KC_TRANSPARENT, KC_DOT,         KC_0,           KC_EQUAL,       KC_TRANSPARENT,
    RGB_VAD,        RGB_VAI,        TOGGLE_LAYER_COLOR,                RGB_SLD,        RGB_HUD,        RGB_HUI
  ),
  [STENO] = LAYOUT_moonlander(
    KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, QK_STENO_BOLT,                                  QK_STENO_GEMINI,STN_FN,         STN_RES1,       STN_RES2,       KC_TRANSPARENT, KC_TRANSPARENT, STN_PWR,
    KC_TRANSPARENT, STN_N1,         STN_N2,         STN_N3,         STN_N4,         STN_N5,         KC_TRANSPARENT,                                 KC_TRANSPARENT, STN_N6,         STN_N7,         STN_N8,         STN_N9,         STN_NA,         STN_NB,
    KC_TRANSPARENT, STN_S1,         STN_TL,         STN_PL,         STN_HL,         STN_ST1,        TG(STENO),                                      KC_TRANSPARENT, STN_ST3,        STN_FR,         STN_PR,         STN_LR,         STN_TR,         STN_DR,
    KC_TRANSPARENT, STN_S2,         STN_KL,         STN_WL,         STN_RL,         STN_ST2,                                        STN_ST4,        STN_RR,         STN_LR,         STN_GR,         STN_SR,         STN_ZR,
    KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, STN_NC,         STN_NC,                                                                                                         STN_NC,         STN_NC,         KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,
    STN_A,          STN_O,          KC_TRANSPARENT,                 KC_TRANSPARENT, STN_E,          STN_U
  ),
  [MEDIA] = LAYOUT_moonlander(
    KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,                                 KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,     KC_TRANSPARENT, KC_TRANSPARENT, RESET,
    KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_MS_UP,       KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,                                 KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,     KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,
    KC_TRANSPARENT, KC_TRANSPARENT, KC_MS_LEFT,     KC_MS_DOWN,     KC_MS_RIGHT,    KC_TRANSPARENT, TG(MEDIA),                                      KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,     KC_TRANSPARENT, KC_TRANSPARENT, KC_MEDIA_PLAY_PAUSE,
    KC_TRANSPARENT, AU_TOG,         MU_TOG,         MU_MOD,         KC_TRANSPARENT, KC_TRANSPARENT,                                                                 KC_TRANSPARENT, KC_TRANSPARENT, KC_MEDIA_PREV_TRACK,KC_MEDIA_NEXT_TRACK,KC_TRANSPARENT, KC_TRANSPARENT,
    KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT, KC_MS_BTN1,     KC_MS_BTN2,     KC_TRANSPARENT,                                                                                                 KC_TRANSPARENT,     KC_AUDIO_VOL_UP,KC_AUDIO_VOL_DOWN,KC_AUDIO_MUTE,  KC_TRANSPARENT, KC_TRANSPARENT,
    KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT,                 KC_TRANSPARENT, KC_TRANSPARENT, KC_TRANSPARENT
  ),
};

extern bool g_suspend_state;
extern rgb_config_t rgb_matrix_config;

void keyboard_post_init_user(void) {
  rgb_matrix_enable();
}

// n.b: layers with per-led colors should use something else.
const uint8_t PROGMEM ledmap_layer[][3] = {
    [BASE_LIN] = { 205, 170, 255 },
    [BASE_MAC] = { 230, 191, 191 },
    [ONESHOT] = { 87, 118, 144 },
    [GAMING] = { 111, 191, 191 },
    [SYMBOLS] = { 137, 191, 127 },
    [STENO] = { 129, 174, 146 },
    [MEDIA] = { 200, 177, 193 },
};

void set_layer_color(int layer) {
    HSV hsv = {
      .h = pgm_read_byte(&ledmap_layer[layer][0]),
      .s = pgm_read_byte(&ledmap_layer[layer][1]),
      .v = pgm_read_byte(&ledmap_layer[layer][2]),
    };

    if (!hsv.h && !hsv.s && !hsv.v) {
        rgb_matrix_set_color_all( 0, 0, 0 );
    } else {
        RGB rgb = hsv_to_rgb( hsv );
        float f = (float)rgb_matrix_config.hsv.v / UINT8_MAX;
        rgb_matrix_set_color_all( f * rgb.r, f * rgb.g, f * rgb.b );
    }
}

void rgb_matrix_indicators_user(void) {
  if (g_suspend_state || keyboard_config.disable_layer_led) { return; }
  uint8_t highest_layer = get_highest_layer(layer_state);

  if (highest_layer >= LOWEST_UNUSED) { return; }

  set_layer_color(highest_layer);
}

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
  switch (keycode) {
    case RGB_SLD:
      if (record->event.pressed) {
        rgblight_mode(1);
      }
      return false;
    case HSV_0_255_255:
      if (record->event.pressed) {
        rgblight_mode(1);
        rgblight_sethsv(0,255,255);
      }
      return false;
    case HSV_86_255_128:
      if (record->event.pressed) {
        rgblight_mode(1);
        rgblight_sethsv(86,255,128);
      }
      return false;
    case HSV_172_255_255:
      if (record->event.pressed) {
        rgblight_mode(1);
        rgblight_sethsv(172,255,255);
      }
      return false;
  }
  return true;
}

enum {
    SINGLE_TAP = 1,
    SINGLE_HOLD,
    MORE_TAPS
};

uint8_t dance_step(qk_tap_dance_state_t *state);

uint8_t dance_step(qk_tap_dance_state_t *state) {
    if (state->count == 1) {
        if (state->interrupted || !state->pressed) return SINGLE_TAP;
        else return SINGLE_HOLD;
    }
    return MORE_TAPS;
}

void on_dance_single_sym_each_tap(qk_tap_dance_state_t *state, void *user_data) {
    qk_tap_dance_pair_t *pair = (qk_tap_dance_pair_t *)user_data;
    if(state->count == 2) {
        tap_code16(pair->kc1);
        tap_code16(pair->kc1);
    }
    if (state->count > 2) {
        tap_code16(pair->kc1);
    }
}

void dance_single_sym_finished(qk_tap_dance_state_t *state, void *user_data) {
    qk_tap_dance_pair_t *pair = (qk_tap_dance_pair_t *)user_data;
    uint8_t step = dance_step(state);
    switch (step) {
        case SINGLE_TAP: register_code16(pair->kc1); break;
        case SINGLE_HOLD: register_code16(pair->kc2); break;
    }
}

void dance_single_sym_reset(qk_tap_dance_state_t *state, void *user_data) {
    qk_tap_dance_pair_t *pair = (qk_tap_dance_pair_t *)user_data;
    wait_ms(10);
    uint8_t step = dance_step(state);
    switch (step) {
        case SINGLE_TAP: unregister_code16(pair->kc1); break;
        case SINGLE_HOLD: unregister_code16(pair->kc2); break;
    }
}

#define TAP_DANCE_2K(kc1,kc2) \
        { .fn = {on_dance_single_sym_each_tap, dance_single_sym_finished, dance_single_sym_reset}, .user_data = (void *)&((qk_tap_dance_pair_t){kc1, kc2}), }

// n.b this could be more efficient, but, eh.
#define TAP_DANCE_LCTL(kc1) TAP_DANCE_2K(kc1, LCTL(kc1))
// n.b see above
#define TAP_DANCE_LCMD(kc1) TAP_DANCE_2K(kc1, LCMD(kc1))

qk_tap_dance_action_t tap_dance_actions[] = {
        [DANCE_LCTL_EQUAL] = TAP_DANCE_LCTL(KC_EQUAL),
        [DANCE_LCTL_DELETE] = TAP_DANCE_LCTL(KC_DELETE),
        [DANCE_LCTL_Q] = TAP_DANCE_LCTL(KC_Q),
        [DANCE_LCTL_W] = TAP_DANCE_LCTL(KC_W),
        [DANCE_LCTL_E] = TAP_DANCE_LCTL(KC_E),
        [DANCE_LCTL_R] = TAP_DANCE_LCTL(KC_R),
        [DANCE_LCTL_T] = TAP_DANCE_LCTL(KC_T),
        [DANCE_LCTL_BSPACE] = TAP_DANCE_LCTL(KC_BSPACE),
        [DANCE_LCTL_A] = TAP_DANCE_LCTL(KC_A),
        [DANCE_LCTL_S] = TAP_DANCE_LCTL(KC_S),
        [DANCE_LCTL_D] = TAP_DANCE_LCTL(KC_D),
        [DANCE_LCTL_F] = TAP_DANCE_LCTL(KC_F),
        [DANCE_LCTL_G] = TAP_DANCE_LCTL(KC_G),
        [DANCE_LCTL_Z] = TAP_DANCE_LCTL(KC_Z),
        [DANCE_LCTL_X] = TAP_DANCE_LCTL(KC_X),
        [DANCE_LCTL_C] = TAP_DANCE_LCTL(KC_C),
        [DANCE_LCTL_V] = TAP_DANCE_LCTL(KC_V),
        [DANCE_LCTL_B] = TAP_DANCE_LCTL(KC_B),
        [DANCE_LCTL_GRAVE] = TAP_DANCE_LCTL(KC_GRAVE),
        [DANCE_LCTL_LEFT] = TAP_DANCE_LCTL(KC_LEFT),
        [DANCE_LCTL_RIGHT] = TAP_DANCE_LCTL(KC_RIGHT),
        [DANCE_LCTL_MINUS] = TAP_DANCE_LCTL(KC_MINUS),
        [DANCE_LCTL_TAB] = TAP_DANCE_LCTL(KC_TAB),
        [DANCE_LCTL_Y] = TAP_DANCE_LCTL(KC_Y),
        [DANCE_LCTL_U] = TAP_DANCE_LCTL(KC_U),
        [DANCE_LCTL_I] = TAP_DANCE_LCTL(KC_I),
        [DANCE_LCTL_O] = TAP_DANCE_LCTL(KC_O),
        [DANCE_LCTL_P] = TAP_DANCE_LCTL(KC_P),
        [DANCE_LCTL_BSLASH] = TAP_DANCE_LCTL(KC_BSLASH),
        [DANCE_LCTL_H] = TAP_DANCE_LCTL(KC_H),
        [DANCE_LCTL_J] = TAP_DANCE_LCTL(KC_J),
        [DANCE_LCTL_K] = TAP_DANCE_LCTL(KC_K),
        [DANCE_LCTL_L] = TAP_DANCE_LCTL(KC_L),
        [DANCE_LCTL_SCOLON] = TAP_DANCE_LCTL(KC_SCOLON),
        [DANCE_LCTL_QUOTE] = TAP_DANCE_LCTL(KC_QUOTE),
        [DANCE_LCTL_N] = TAP_DANCE_LCTL(KC_N),
        [DANCE_LCTL_M] = TAP_DANCE_LCTL(KC_M),
        [DANCE_LCTL_COMMA] = TAP_DANCE_LCTL(KC_COMMA),
        [DANCE_LCTL_DOT] = TAP_DANCE_LCTL(KC_DOT),
        [DANCE_LCTL_SLASH] = TAP_DANCE_LCTL(KC_SLASH),
        [DANCE_LCTL_UP] = TAP_DANCE_LCTL(KC_UP),
        [DANCE_LCTL_DOWN] = TAP_DANCE_LCTL(KC_DOWN),
        [DANCE_LCTL_LBRACKET] = TAP_DANCE_LCTL(KC_LBRACKET),
        [DANCE_LCTL_RBRACKET] = TAP_DANCE_LCTL(KC_RBRACKET),
        [DANCE_LCMD_EQUAL] = TAP_DANCE_LCMD(KC_EQUAL),
        [DANCE_LCMD_DELETE] = TAP_DANCE_LCMD(KC_DELETE),
        [DANCE_LCMD_Q] = TAP_DANCE_LCMD(KC_Q),
        [DANCE_LCMD_W] = TAP_DANCE_LCMD(KC_W),
        [DANCE_LCMD_E] = TAP_DANCE_LCMD(KC_E),
        [DANCE_LCMD_R] = TAP_DANCE_LCMD(KC_R),
        [DANCE_LCMD_T] = TAP_DANCE_LCMD(KC_T),
        [DANCE_LCMD_BSPACE] = TAP_DANCE_LCMD(KC_BSPACE),
        [DANCE_LCMD_A] = TAP_DANCE_LCMD(KC_A),
        [DANCE_LCMD_S] = TAP_DANCE_LCMD(KC_S),
        [DANCE_LCMD_D] = TAP_DANCE_LCMD(KC_D),
        [DANCE_LCMD_F] = TAP_DANCE_LCMD(KC_F),
        [DANCE_LCMD_G] = TAP_DANCE_LCMD(KC_G),
        [DANCE_LCMD_Z] = TAP_DANCE_LCMD(KC_Z),
        [DANCE_LCMD_X] = TAP_DANCE_LCMD(KC_X),
        [DANCE_LCMD_C] = TAP_DANCE_LCMD(KC_C),
        [DANCE_LCMD_V] = TAP_DANCE_LCMD(KC_V),
        [DANCE_LCMD_B] = TAP_DANCE_LCMD(KC_B),
        [DANCE_LCMD_GRAVE] = TAP_DANCE_LCMD(KC_GRAVE),
        [DANCE_LCMD_LEFT] = TAP_DANCE_LCMD(KC_LEFT),
        [DANCE_LCMD_RIGHT] = TAP_DANCE_LCMD(KC_RIGHT),
        [DANCE_LCMD_MINUS] = TAP_DANCE_LCMD(KC_MINUS),
        [DANCE_LCMD_TAB] = TAP_DANCE_LCMD(KC_TAB),
        [DANCE_LCMD_Y] = TAP_DANCE_LCMD(KC_Y),
        [DANCE_LCMD_U] = TAP_DANCE_LCMD(KC_U),
        [DANCE_LCMD_I] = TAP_DANCE_LCMD(KC_I),
        [DANCE_LCMD_O] = TAP_DANCE_LCMD(KC_O),
        [DANCE_LCMD_P] = TAP_DANCE_LCMD(KC_P),
        [DANCE_LCMD_BSLASH] = TAP_DANCE_LCMD(KC_BSLASH),
        [DANCE_LCMD_H] = TAP_DANCE_LCMD(KC_H),
        [DANCE_LCMD_J] = TAP_DANCE_LCMD(KC_J),
        [DANCE_LCMD_K] = TAP_DANCE_LCMD(KC_K),
        [DANCE_LCMD_L] = TAP_DANCE_LCMD(KC_L),
        [DANCE_LCMD_SCOLON] = TAP_DANCE_LCMD(KC_SCOLON),
        [DANCE_LCMD_QUOTE] = TAP_DANCE_LCMD(KC_QUOTE),
        [DANCE_LCMD_N] = TAP_DANCE_LCMD(KC_N),
        [DANCE_LCMD_M] = TAP_DANCE_LCMD(KC_M),
        [DANCE_LCMD_COMMA] = TAP_DANCE_LCMD(KC_COMMA),
        [DANCE_LCMD_DOT] = TAP_DANCE_LCMD(KC_DOT),
        [DANCE_LCMD_SLASH] = TAP_DANCE_LCMD(KC_SLASH),
        [DANCE_LCMD_UP] = TAP_DANCE_LCMD(KC_UP),
        [DANCE_LCMD_DOWN] = TAP_DANCE_LCMD(KC_DOWN),
        [DANCE_LCMD_LBRACKET] = TAP_DANCE_LCMD(KC_LBRACKET),
        [DANCE_LCMD_RBRACKET] = TAP_DANCE_LCMD(KC_RBRACKET),
};

void matrix_init_user() {
  steno_set_mode(STENO_MODE_GEMINI);
}
